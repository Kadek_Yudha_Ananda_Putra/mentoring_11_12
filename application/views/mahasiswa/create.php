<!DOCTYPE html>
<html>
<head>
    <title>Tambah Mahasiswa</title>
    <!-- Tambahkan link CSS Bootstrap dan Select2 di sini -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css">
</head>
<body>
    <div class="container">
        <h1>Tambah Mahasiswa</h1>
        <form method="post" action="<?php echo site_url('mahasiswa/store'); ?>">
            <div class="form-group">
                <label for="nim">NIM:</label>
                <input type="text" class="form-control" id="nim" name="nim" required>
            </div>
            <div class="form-group">
                <label for="nama">Nama:</label>
                <input type="text" class="form-control" id="nama" name="nama" required>
            </div>
            <div class="form-group">
                <label for="jenis_kelamin">Jenis Kelamin:</label>
                <select class="form-control" id="jenis_kelamin" name="jenis_kelamin" required>
                    <option value="L">Laki-laki</option>
                    <option value="P">Perempuan</option>
                </select>
            </div>
            <div class="form-group">
                <label for="alamat">Alamat:</label>
                <textarea class="form-control" id="alamat" name="alamat" required></textarea>
            </div>
            <div class="form-group">
                <label for="hobi">Hobi:</label>
                <select multiple class="form-control" id="hobi" name="hobi[]">
                    <?php foreach ($hobi as $row): ?>
                        <option value="<?php echo $row['id']; ?>"><?php echo $row['hobi']; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href="<?php echo site_url('mahasiswa'); ?>" class="btn btn-secondary">Kembali</a>
        </form>
    </div>
    <!-- Tambahkan script JS Bootstrap dan Select2 di sini -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js"></script>
    <script>
        $(document).ready(function() {
            // Inisialisasi Select2 pada elemen select dengan ID "hobi"
            $('#hobi').select2();
        });
    </script>
</body>
</html>
