<!DOCTYPE html>
<html>
<head>
    <title>Daftar Mahasiswa</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h1>Daftar Mahasiswa</h1>
        <a href="<?php echo site_url('mahasiswa/create'); ?>" class="btn btn-primary">Tambah Mahasiswa</a>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>NIM</th>
                    <th>Nama</th>
                    <th>Jenis Kelamin</th>
                    <th>Alamat</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($mahasiswas as $mahasiswa): ?>
                    <tr>
                        <td><?php echo $mahasiswa['nim']; ?></td>
                        <td><?php echo $mahasiswa['nama']; ?></td>
                        <td><?php echo $mahasiswa['jenis_kelamin']; ?></td>
                        <td><?php echo $mahasiswa['alamat']; ?></td>
                        <td>
                            <a href="<?php echo site_url('mahasiswa/edit/'.$mahasiswa['id']); ?>" class="btn btn-warning">Edit</a>
                            <a href="<?php echo site_url('mahasiswa/delete/'.$mahasiswa['id']); ?>" class="btn btn-danger">Hapus</a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</body>
</html>
