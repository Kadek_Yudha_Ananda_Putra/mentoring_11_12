<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Upload File</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
		body{
			background-color: #f5f5f5;
		}

        .custom-card {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            transition: 0.3s;
            border-radius: 5px;
            padding: 20px;
        }

        .custom-card:hover {
            box-shadow: 0 8px 16px 0 rgba(0, 0, 0, 0.2);
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="card custom-card">
                    <div class="card-body">
                        <h2 class="card-title text-center">Upload Multiple Files</h2>

                        <?php if (isset($error)) { ?>
                            <div class="alert alert-danger"><?= $error; ?></div>
                        <?php } ?>

                        <?= form_open_multipart('UploadFiles/do_upload'); ?>
                        <div class="form-group">
                            <label for="userfile">Pilih File:</label>
                            <input type="file" name="userfile[]" id="userfile" class="form-control" multiple />
                        </div>
                        <input type="submit" value="Upload" class="btn btn-primary" />
                        </form>

                        <h3 class="text-center">Daftar File yang Diunggah</h3>

                        <?php if (empty($files)) { ?>
                            <p>Tidak ada file yang diunggah.</p>
                        <?php } else { ?>
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Nama File</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($files as $file) { ?>
                                        <tr>
                                            <td><?= $file->file_name; ?></td>
                                            <td>
                                                <a href="<?= base_url('UploadFiles/delete/' . $file->id); ?>" class="btn btn-danger btn-sm" onclick="return confirm('Apakah Anda yakin ingin menghapus file ini?')">Hapus</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
