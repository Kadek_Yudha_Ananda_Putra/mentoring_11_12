<?php
class Mahasiswa_model extends CI_Model {
    public function __construct() {
        $this->load->database();
    }

    public function create($data) {
        $this->db->trans_start();
        $this->db->insert('mahasiswa', $data['mahasiswa']);
        $mahasiswa_id = $this->db->insert_id();

        foreach ($data['hobi'] as $hobi_id) {
            $this->db->insert('mahasiswa_hobi', ['id_mahasiswa' => $mahasiswa_id, 'id_hobi' => $hobi_id]);
        }

        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function update($id, $data) {
        $this->db->trans_start();
        $this->db->update('mahasiswa', $data['mahasiswa'], ['id' => $id]);

        $this->db->delete('mahasiswa_hobi', ['id_mahasiswa' => $id]);

        foreach ($data['hobi'] as $hobi_id) {
            $this->db->insert('mahasiswa_hobi', ['id_mahasiswa' => $id, 'id_hobi' => $hobi_id]);
        }

        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function delete($id) {
        $this->db->trans_start();
        $this->db->delete('mahasiswa_hobi', ['id_mahasiswa' => $id]);
        $this->db->delete('mahasiswa', ['id' => $id]);
        $this->db->trans_complete();

        return $this->db->trans_status();
    }

    public function get_all() {
        return $this->db->get('mahasiswa')->result_array();
    }

    public function ref_hobi() {
        return $this->db->get('ref_hobi')->result_array();
    }

    public function get_by_id($id) {
        return $this->db->get_where('mahasiswa', ['id' => $id])->row_array();
    }

    public function get_mahasiswa_hobi($id) {
        return $this->db->get_where('mahasiswa_hobi', ['id_mahasiswa' => $id])->result();
    }
}
